#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h> 
#include <netinet/in.h>

    
    
    //Creamos el socket parecido al chat
    int create_tcp_socket();

    //Se busca la IP del host
    char *get_ip(char *host);

    //Se arma el request HTTP
    char *build_get_query(char *host, char *page, int pido);

    // Un método que le dice al usuario como usar el programa
    void usage();
    
    //Metodo para obtener el sizeUrl
    char *obtenerUrl(char *msje);

    //Metodo para ver si hay un espacio en el URL ingresado por el cliente
    int hayEspacioEnURL(char *msje);

    //Metodo que retorna si la ip ingresada es una ip valida para conectarse
    int esValidaIP(char *ip_del_cliente);


    // Se define un host por defecto-----
    #define HOST "www.uc.cl"
    #define PAGE "/"
    #define PORT 80
    //#define USERAGENT "HTMLGET 1.0"
    //---------------------------

    int esValidaIP(char *ip_del_cliente)
    {
    	char const* const fileName = "Valid_IP.txt";
    	FILE* file = fopen(fileName, "r"); 
    	char line[256];
    	char *cip = ip_del_cliente;
    	int sizeIP = strlen(cip);    	
    	int esValida = 0;
    	//revisamos linea por linea
    	while (fgets(line, sizeof(line), file)&& (esValida==0)) 
    	{
    		
  			char *ipTexto = line;
  			int var_aux = 1;
  			for(int i=0; i<sizeIP;i++)
  			{
  				//si tiene algun caracter distinto continuamos con la sigte line
  				if(cip[i]!=ipTexto[i])
  				{
  					var_aux = 0;
  					break;;
  				}
  			}
  			if(var_aux==1)
  			{	
  				//nos aseguramos que la ip no tenga mas caracteres validos en una ip
  				if(ipTexto[sizeIP]=='0'||ipTexto[sizeIP]=='1'||ipTexto[sizeIP]=='2'||ipTexto[sizeIP]=='3'
  					||ipTexto[sizeIP]=='4'||ipTexto[sizeIP]=='5'||ipTexto[sizeIP]=='6'||ipTexto[sizeIP]=='7'
  					||ipTexto[sizeIP]=='8'||ipTexto[sizeIP]=='9'||ipTexto[sizeIP]=='.')
  				{
  					continue;
  				}
  				esValida=1;
  			}
  					
    	}
    	fclose(file);       	
       	return esValida;       	
    }


    int hayEspacioEnURL(char *msje)
    {
    	int size = strlen(msje)-1;
    	int termino = 0;
    	while(msje[size-1-termino]!='0')
        {
            	termino++;
        }
        size = size - termino;
    	int aux=5;
    	if(msje[0]=='G')
    		aux=4;
    	for(int i=aux; i<size-9;i++)
        {
           	//hay un espacio entremedio asi que retorno que no es valido
            if(msje[i]==' ')
            	return 1;
        }
        return 0;
    }


    char *obtenerUrl(char *msje)
    {
        int size = strlen(msje) - 1;
        int termino = 0;
    	while(msje[size-1-termino]!='0')
        {
            	termino++;
        }
        size = size - termino;
        int aux = 5;
        if(msje[0]=='G')
        	aux=4;
        char *url = (char *)malloc(size - 9 - aux);

        for(int i=aux; i<size-9;i++)
        {
            url[i-aux] = msje[i];
        }
        return url;
    }

    //Le digo al usuario como usar mi programa
    void usage()
    {
    	fprintf(stderr, "USO: despues del ./http ingrese un puerto valido\n\
    	\tpuerto: El puerto tiene que ser un numero mayor a 1024\n");
    }
     
     
     // Creo el socket
    int create_tcp_socket()
    {
      int sock;
      //Acá se setea el socket y se buscan errores
      if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
      {
        perror("No se puede crear  el socket");
        exit(0);
      }
      return sock;
    }
     
     
     //Obtendo la IP
    char *get_ip(char *host)
    {
    	//printf("%s\n", host);
    	// hostent libería netdb
      	struct hostent *hent;

      	//El largo de la IP
      	int iplen = 15; //XXX.XXX.XXX.XXX

      	//Se crea un string del largo de la ip que va a ser lo que devolveremos
      	char *ip = (char *)malloc(iplen+1);

      	//Seteamos un trozo de memoria
      	memset(ip, 0, iplen+1);

      	
      	//hent = gethostbyname(host);

      	// Se crea el "objeto hostent hent" en caso que la pagina sea valida
      	if((hent = gethostbyname(host)) != NULL)
      	{
      		// magia netdb que setea la ip que obtuvo del host
      		inet_ntop(AF_INET, (void *)hent->h_addr_list[0], ip, iplen);
      	}
      	//Si la pagina no era valida, lo guardamos como null
      	else
      	{
      		ip = NULL;
      	}
      	// magia netdb que setea la ip que obtuvo del host
      	//inet_ntop(AF_INET, (void *)hent->h_addr_list[0], ip, iplen);
      
      	return ip;
    }
     




     //Armo la solicitud -Podría modificarse...
    char *build_get_query(char *host, char *page, int pido)
    {
    	//El string de la query
      	char *query;
      	//La página solicitada
      	char *getpage = page;

      	char *tpl;

      	char *USERAGENT;

      	//La solicitud GET, esto lo tienene que modificar porque tiene que llegar desde el cliente a su proxy

      	//si pido es 0 es porque es HEAD
      	if(pido== 0)
      	{
      		tpl = "HEAD /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n";
      		USERAGENT = "HTMLHEAD 1.0";
      	}
      	

      	else if(pido==1)
      	{
      		tpl = "POST /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n";
      		USERAGENT = "HTMLPOST 1.0";
      	}

      	else
      	{
      		tpl = "GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n";
      		USERAGENT = "HTMLGET 1.0";
      	}

      	// Qué pasará si pongo HEAD???????!

      	if(getpage[0] == '/')
      	{
      	  getpage = getpage + 1;
      	  //fprintf(stderr," \"/\",  %s %s\n", page, getpage);
      	}

      	//Creamos el string query vacío
      	query = (char *)malloc(strlen(host)+strlen(getpage)+strlen(USERAGENT)+strlen(tpl)-5);

      	//Se setea acá:
      	sprintf(query, tpl, getpage, host, USERAGENT);
      	return query;
    }














    //METODO PRINCIPAL
    int main(int argc, char **argv)
    {
    	//Lo necesario para conectarse con las paginas
        
      	// Igual que el chat se arma un socket
      	struct sockaddr_in *remote;
      	// El puerto del socket
      	int sock;
      	// ALGP
      	int tmpres;
      	//la ip como string
      	char *ip;
      
      	//La query final
      	char *get;
      	// El buffer que recibirá el archivo
    	int sizeBuffer = 512;
      	char buf[sizeBuffer+1];
      	// El host al cual se hace l solicitud
      	char *host;
      	// el recurso solicitado (Ej index.html)
      	char *page;
     
      	page = PAGE;
      
      	// Lo necesario para la coneccion cliente servidor      

		//Descriptor del servidor
		int server_socket_descriptor;
		//Descriptor del cliente cliente
		int client_socket_descriptor;
		//Puerto:
		int port_number;
		//ALGO
		socklen_t client_lenght;
		//Un buffer para enviar info
		char buffer[sizeBuffer+1];
		//Estructura del servidor
		struct sockaddr_in server_addr;
		//Estructura cliente
		struct sockaddr_in  client_addr;

		//Esto sirve para leer el futuro mesaje del cliente
		int n;

		// Controlamos el error de no proveer el puerto para el servidor
		if (argc < 2) 
		{
			//Le decimos al usuario como usar el programa
            usage();
			exit(0);
		}

		



		// Creamos el socket del servidor
		server_socket_descriptor = create_tcp_socket();


		// Especificamos los detalles para el servidor
		//La  función  bzero() pone a cero los primeros 'sizeof(server_addr)' bytes del área de bytes que comienza en &server_addr.
		bzero((char *) &server_addr, sizeof(server_addr));

		//Tomamos el puerto dado por el segundo argumento del programa
		port_number = atoi(argv[1]);

		// Se setean los valores del struct
		 server_addr.sin_family = AF_INET;
	 	// Que ips vamos a escuchar, si queremos una especifica podemos poner: sin.sin_addr.s_addr = inet_addr("xxx.xxx.xxx.xxx");
		 server_addr.sin_addr.s_addr = INADDR_ANY;

	 	// htons() transforma la orden en bytes para redes.
	 	server_addr.sin_port = htons(port_number);

		// Registramos el socket
		if (bind(server_socket_descriptor, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) 
			error("ERROR on binding");
        
        //defino exit como false (hasta que el client ponga exit)
        int salir= 0;
        
        //salida = 
        while(salir==0)
        {
            // Comenzamos a escuchar por el socket, 5 es la cantidad de procesos que pueden estar esperando mientra se establece la conexion.		
            
            listen(server_socket_descriptor,5);
            printf("\n -- Server listening --\n");	
            
            // Obtenemos el largo del mensaje
            client_lenght = sizeof(client_addr);
            
            // Aceptamos la conexion de algun cliente
            client_socket_descriptor = accept(server_socket_descriptor, 
                                              (struct sockaddr *) &client_addr, 
                                              &client_lenght);
            
            // Si no se puede obtener el descriptor, enviamos un error
            if (client_socket_descriptor < 0) 
                error("ERROR on accept");
            
            //Desde aca estoy modificando


            struct sockaddr_in* pV4Addr = (struct sockaddr_in*)&client_addr;
			int ipAddr = pV4Addr->sin_addr.s_addr;
			char ip_cliente[INET_ADDRSTRLEN];
			inet_ntop( AF_INET, &ipAddr, ip_cliente, INET_ADDRSTRLEN );
			//printf("%s\n", ip_cliente);

			
			
			//No es valida la ip del cliente
			if(esValidaIP(ip_cliente)==0)
			{
				char respuesta[256] = "\n400 BAD \nSu IP no tiene los permisos para utilizar el proxy. \n"; 
            	printf("%s\n", respuesta);
            	n = write(client_socket_descriptor, respuesta, 256);
	                      
	            // Si no se pudo escribir, enviamos un error
	            if (n < 0) 
	            	error("ERROR writing to socket");

	            close(client_socket_descriptor);
                continue;
			}
			
			
			


			//Hasta aca

            // Comenzamos la lectura del mensaje	
            bzero(buffer,sizeBuffer+1);
            n = read(client_socket_descriptor,buffer,sizeBuffer);
            
            // Controlamos el error de lectura del socket
            if (n < 0) 
                error("ERROR reading from socket");
            
            char* msje = &buffer[0];
            int sizeMsje = strlen(msje)-1;
            printf("%c\n", msje[sizeMsje-1]);

            
            
            // Impimimos el mensaje enviado
            printf("Here is the message: %s\n",buffer);
            int termino = 0;

            while(msje[sizeMsje-1-termino]!='0')
            {
            	termino++;
            }
            sizeMsje = sizeMsje - termino;

            //Veo especificamente que parte esta mala
            //Si no parte con GET, HEAD o POST esta malo
            if((!(msje[0]=='G' && msje[1]=='E' && msje[2]=='T' && msje[3]==' '))&&
            	(!(msje[0]=='H' && msje[1]=='E' && msje[2]=='A' && msje[3]=='D' && msje[4]==' '))&&
            	(!(msje[0]=='P' && msje[1]=='O' && msje[2]=='S' && msje[3]=='T' && msje[4]==' ')))
            {
            	char respuesta[256] = "\n400 BAD \nProblema con la preposicion. Asegurese que sea alguno de los siguientes tipos: \nHEAD \nGET \nPOST \n"; 
            	printf("%s\n", respuesta);
            	n = write(client_socket_descriptor, respuesta, 256);
	                      
	            // Si no se pudo escribir, enviamos un error
	            if (n < 0) 
	            	error("ERROR writing to socket");
            }
            //Si no termina con HTTP/1.0 esta malo
            else if(!(msje[sizeMsje-1]=='0'&& msje[sizeMsje-2]=='.'&& msje[sizeMsje-3]=='1'&& msje[sizeMsje-4]=='/'&& 
            	msje[sizeMsje-5]=='P'&& msje[sizeMsje-6]=='T'&& msje[sizeMsje-7]=='T'&& msje[sizeMsje-8]=='H'&& msje[sizeMsje-9]==' '))
            {
            	char respuesta[256] = "\n400 BAD \nProblema con el final. Asegurese que termine con: \n' HTTP/1.0' \n"; 
            	printf("%s\n", respuesta);
            	n = write(client_socket_descriptor, respuesta, 256);
	                      
	            // Si no se pudo escribir, enviamos un error
	            if (n < 0) 
	            	error("ERROR writing to socket");
            }
            //Si no tiene ningun url esta malo
            else if(3== sizeMsje-9)
            {
            	char respuesta[256] = "\n400 BAD\nHa ingresado un URL vacio. Asegurese de ingresar un URL. \n"; 
            	printf("%s\n", respuesta);
            	n = write(client_socket_descriptor, respuesta, 256);
	                      
	            // Si no se pudo escribir, enviamos un error
	            if (n < 0) 
	            	error("ERROR writing to socket");
            }
            //Si es que hay un espacio entremedio del url esta malo
            else if(hayEspacioEnURL(msje) == 1)
            {
            	char respuesta[256] = "\n400 BAD\nHa ingresado un URL no valido. Asegurese de ingresar un URL sin espacios entremedio. \n"; 
            	printf("%s\n", respuesta);
            	n = write(client_socket_descriptor, respuesta, 256);
	                      
	            // Si no se pudo escribir, enviamos un error
	            if (n < 0) 
	            	error("ERROR writing to socket");
            }
            
            //Si todo lo de arriba cumple, se analiza el request
            else
            {
                //el tamano del url va a ser el largo del msje menos 
                //todas las letras necesarias del protocolo (GET  HTTP/1.0)
                int largo_descontar = 5;
                if(msje[0]=='G')
                	largo_descontar=4;
                int sizeUrl = strlen(msje) - 9 - largo_descontar;
        
                char *url = (char *)malloc(sizeUrl);
                url = obtenerUrl(msje);
                FILE *request;
                char *rutaLeer[strlen(url)+1];
                strcpy(rutaLeer,url);
                //Por default digo que es GET
              	int begin = 2;
                if(msje[0]=='H')
	            {
	            	begin = 0;
	            	strcat(rutaLeer,"HEAD.txt");

	            }

	            else if(msje[0]=='P')
	            {
	            	begin = 1;
	            	strcat(rutaLeer,"POST.txt");
	            }

	            else
	            {
	            	begin = 2;
	            	strcat(rutaLeer,"GET.txt");
	            }
                request = fopen(rutaLeer,"r");

                if(request != NULL)
                {
                	printf("Esta en cache\n");
                	size_t aux;
                	char bufferLeer[sizeBuffer+1];
                	memset(bufferLeer,0,sizeBuffer+1);
                	while((aux=fread(bufferLeer,1,sizeBuffer+1,request))>0)
                	{
                		fwrite(bufferLeer,1,aux,stdout);
                		//fprintf(stdout, bufferLeer);
                		// Enviamos confirmacion al cliente
	                    n = write(client_socket_descriptor, bufferLeer,sizeBuffer);
	                      
	                    // Si no se pudo escribir, enviamos un error
	                    if (n < 0) 
	                        error("ERROR writing to socket");
                	}
                	fclose(request);

                }
                
                else
                {
                	printf("No esta en cache\n");
                	// Obtenemos la ip del host (ver método arriba)
                	ip = get_ip(url);

                	//SI el url ingresado no existe, se tira un error
                	if(ip == NULL)
                	{	
                		//printf("%s\n",ip );
                		char respuesta[256] = "\n400 BAD\nLa pagina web ingresada no existe. Porfavor asegurese de ingresar una que exista.\n"; 
            			printf("%s\n", respuesta);
            			n = write(client_socket_descriptor, respuesta, 256);
	                      
	            		// Si no se pudo escribir, enviamos un error
	            		if (n < 0) 
	            			error("ERROR writing to socket");

	            		//Aca podria hacer un archivo con la pagina mala para agregarla al cache pero si 
	            		//esta mala me parece que no vale la pena (el codigo seria igual que abajo)

                		close(client_socket_descriptor);
                		continue;
                	}
                
                	//Le decimos al usuario la IP
	                fprintf(stderr, "La IP del host es: %s\n", ip);
	                
	                //Creamos el socket (ver método arriba)
	              	sock = create_tcp_socket();

	              	// remote es un sockaddr_in, se crea un nuevo "objeto" de ete tipo 
	              	remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in *));


	              	remote->sin_family = AF_INET;

	              	// Obtenemos un elemento que valida el estado
	              	tmpres = inet_pton(AF_INET, ip, (void *)(&(remote->sin_addr.s_addr)));


	              	//Según el valor del tmpres le digo al usuario que es lo que pasa ------------
	              	if( tmpres < 0)  
	              	{
	                	perror("No se puede setear remote->sin_addr.s_addr");
	                	exit(1);
	              	}
	              	else if(tmpres == 0)
	              	{
	                	fprintf(stderr, "%s No es un dirección válida\n", ip);
	                	exit(1);
	              	}
	              	//----------------------------


              		// Lo mismo con el puerto
              		remote->sin_port = htons(PORT);
             
              		if(connect(sock, (struct sockaddr *)remote, sizeof(struct sockaddr)) < 0)
              		{
                		perror("No se pudo conectar");
                		exit(0);
              		}
              		

	              	//Acá le decimos al usuario cuál es su solicitud con el método build query
	              	


	              	get = build_get_query(url, page, begin);

	              	fprintf(stderr, 
	                	"Query is:\n<<SU REQUEST TIENE ESTOS DATOS>>\n%s<<FIN>>\n", get);
	             




	              	//Enviamos la solicitud al servidor:

	              	//Comenzamos a mandar el string de la query
	              	int sent = 0;

	              	while(sent < strlen(get))
	              	{

	                
	                	tmpres = send(sock, get+sent, strlen(get)-sent, 0);


	                	//Nuevamente revisamos algún error:
	                	if(tmpres == -1)
	                	{	
	                  		error("No se puede mandar la solicitud");
	                  		exit(1);
	                	}
	                	sent += tmpres;
	              	}
	              	//Terminamos de mandar la solicitud


	              	//Ahora recibimos nuestro archivo de vuelta

	              	FILE *guardar;
	              	char *rutaGuardar[strlen(url)+1];
	              	strcpy(rutaGuardar,url);
	              	if(begin==0)
	              	{
	              		strcat(rutaGuardar,"HEAD.txt");
	              	}
	              	else if(begin==1)
	              	{
	              		strcat(rutaGuardar,"POST.txt");
	              	}
	              	else
	              	{
	              		strcat(rutaGuardar,"GET.txt");
	              	}
	              	
	              	guardar = fopen(rutaGuardar, "w");



	              	//ACA ES DONDE EL PROXY DEBERÍA GUARDAR LA RESPUESTA




	              	memset(buf, 0, sizeof(buf));
	              	//Lo mismo que antes, se empieza a leer la respuesta
	              	int htmlstart = 0;
	              	//El string recibido
	              	char * htmlcontent;


	              	//importante


	              	while((tmpres = recv(sock, buf, sizeBuffer, 0)) > 0)
	              	{
	              		//printf("\n \n \n El tmpres es igual a: %d \n \n \n ",tmpres);
	                	if(htmlstart == 0)
	                  	{
	                    	//Si lo ocupan así obtendrán solo el archivo
	                      	//htmlcontent = strstr(buf, "\r\n\r\n");
	                      
	                      	//Si lo ocupan así tendrán en HEAD+ El resto del archivo
	                      	htmlcontent = strstr(buf, "");

	                      	if(htmlcontent != NULL)
	                      	{
	                        	htmlstart = 1;
	                        	htmlcontent += 4;
	                      	}
	                  	}
	                  	else
	                  	{
	                    	htmlcontent = buf;
	                  	}

	                  	if(htmlstart)
	                  	{
	                  		//  printf(buf);
	                      	fprintf(stdout, htmlcontent);// Enviamos confirmacion al cliente
	                      	n = write(client_socket_descriptor, htmlcontent,sizeBuffer);
	                      	fwrite(htmlcontent,1,sizeBuffer,guardar);
	                      
	                      	// Si no se pudo escribir, enviamos un error
	                      	if (n < 0) 
	                          	error("ERROR writing to socket");
	                  	}
	             		//printf("\n \n \n El tempres es: %d \n \n \n", tmpres);
	                 	memset(buf, 0, tmpres);
	                 	//bzero(buf,sizeBuffer+1);
	                 	//bzero(buf,tempres);
	              	}
	              	//printf("\n \n \n El tmpres es igual a: %d \n \n \n ",tmpres);


	              	if(tmpres < 0)
	              	{
	                	perror("Error al recibir el contenido");
	              	}
	              	//free(get);
      				//free(remote);
      				//free(ip);
      				//free(htmlcontent);
      				//close(sock);
      				fclose(guardar);
      				//free(rutaGuardar);

	              	

	            }            
	            
	        }
	        close(client_socket_descriptor);
        }
        close(server_socket_descriptor);

        return 0; 
	}