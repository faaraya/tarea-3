/**********************************************
 * IIC2333 - Cliente simple de servidor
 **********************************************/

// Incluimos lo necesario

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

/**
 * Funcion para errores
 */


void error(const char *msg)
{
    perror(msg);
    exit(0);
}


/**
 * Funcion principal
 * Se llama especificando el host y puerto
 */
int main(int argc, char *argv[])
{
    // Definimos las estructuras a ocupar
    int sizeBuffer = 512;
    int sockfd;
    int port_number;
    int n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    // Controlamos el error que no se hayan especificado los datos necesario (phost y puerto)
    char buffer[sizeBuffer+1];
    if (argc < 3) {
       fprintf(stderr,"Usage %s hostname port\n", argv[0]);
       exit(0);
    }

    // Extraemos el puerto
    port_number = atoi(argv[2]);

    // Creamos el socket, controlando el error si es que no puede ser creado
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");

    // Obtenemos el servidor para ver si es valido
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    // Obtenemos los datos para conectarnos al servidor
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(port_number);

    // Intentamos conectarnos al servidor
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");

    
    
    //Solicitamos el mensaje a enviar
    printf("Please enter the message: ");
    bzero(buffer,sizeBuffer+1);
    fgets(buffer,sizeBuffer,stdin);

    // Lo escribimos intentamos enviar
    n = write(sockfd,buffer,strlen(buffer));
    if (n < 0) 
        error("ERROR writing to socket");


    //Comente esto tambien

    // Obtenemos la respuesta 
    //bzero(buffer,sizeBuffer+1);



    //DESDE AQUI AGREGUE WEAS
    memset(buffer, 0, sizeof(buffer));
    //Lo mismo que antes, se empieza a leer la respuesta
    int htmlstart = 0;
    //El string recibido
    char * htmlcontent;

    int tmpres;
        

    while((tmpres = recv(sockfd, buffer, sizeBuffer, 0)) > 0)
    {
        //printf("\n \n \n El tmpres es igual a: %d \n \n \n ",tmpres);
        if(htmlstart == 0)
        {
            //Si lo ocupan así obtendrán solo el archivo
            //htmlcontent = strstr(buffer, "\r\n\r\n");
                      
            //Si lo ocupan así tendrán en HEAD+ El resto del archivo
            htmlcontent = strstr(buffer, "");

            if(htmlcontent != NULL)
            {
                htmlstart = 1;
                htmlcontent += 4;
                //printf("%s", htmlcontent);
            }
            htmlcontent = buffer;
        }
        else
        {
            htmlcontent = buffer;
        }

        if(htmlstart)
        {
            //  printf(buf);
            printf("%s", htmlcontent);// Enviamos confirmacion al cliente
            //printf("\n \n \n El tmpres es igual a: %d \n \n \n ",tmpres);
            //printf("\n \n \n El tamano del buffer es igual a: %d \n \n \n ",sizeBuffer);
                
        }
        //printf("\n \n \n El tempres es: %d \n \n \n", tmpres);
        memset(buffer, 0, tmpres);

        //bzero(buf,sizeBuffer+1);
        //bzero(buf,tempres);
    }
    //printf("\n \n \n El tmpres es igual a: %d \n \n \n ",tmpres);
    //printf("\n \n \n Se termino de recibir el msje \n \n \n ");

    if(tmpres < 0)
    {
        perror("Error al recibir el contenido");
    }
    
    // Cerramos el descriptor
    close(sockfd);
    return 0;
}
