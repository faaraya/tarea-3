/*********************************************/
/* IIC2333 Ejemplo de un servidor simple     */
/*********************************************/

// Incluimos lo necesario

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

/**
 * Funcion para desplegar los errores
 */
void error(const char *msg)
{
	perror(msg);
	exit(1);
}


/**
 * Funcion principal, se llama indicando el puerto de escucha
 */
int main(int argc, char *argv[])
{
	
	// Definimos estructuras necesarias

	//Descriptor del servidor
	int server_socket_descriptor;
	//Descriptor del cliente cliente
	int client_socket_descriptor;
	//Puerto:
	int port_number;
	//ALGO
	socklen_t client_lenght;
	//Un buffer para enviar info
	char buffer[256];
	//Estructura del servidor
	struct sockaddr_in server_addr;
	//Estructura cliente
	struct sockaddr_in  client_addr;

	//Esto sirve para leer el futuro mesaje del cliente
	int n;

	// Controlamos el error de no proveer el puerto para el servidor
	if (argc < 2) {
		fprintf(stderr,"ERROR, no port provided\n");
		exit(1);
	}

	// Creamos el socket del servidor
	server_socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
	/*

    domain. Se podrá establecer como AF_INET (para usar los protocolos de Internet), o como AF_UNIX (si se desea crear sockets para la comunicación interna del sistema). Éstas son las más usadas, pero no las únicas. Existen muchas más, aunque no se nombrarán aquí.

    type. Aquí se debe especificar la clase de socket que queremos usar (de Flujos o de Datagramas). Las variables que deben aparecer son SOCK_STREAM o SOCK_DGRAM según querramos usar sockets de Flujo o de Datagramas, respectivamente.

    protocol. Aquí, simplemente se puede establecer el protocolo a 0. 

	*/
	
	// Controlamos el error de que no haya podido ser creado
	if (server_socket_descriptor < 0) 
		error("ERROR opening socket");


	// Especificamos los detalles para el servidor
	//La  función  bzero() pone a cero los primeros 'sizeof(server_addr)' bytes del área de bytes que comienza en &server_addr.
	bzero((char *) &server_addr, sizeof(server_addr));

	//Tomamos el puerto dado por el segundo argumento del programa
	port_number = atoi(argv[1]);

	// Se setean los valores del struct
	 server_addr.sin_family = AF_INET;
	 // Que ips vamos a escuchar, si queremos una especifica podemos poner: sin.sin_addr.s_addr = inet_addr("xxx.xxx.xxx.xxx");
	 server_addr.sin_addr.s_addr = INADDR_ANY;

	 // htons() transforma la orden en bytes para redes.
	 server_addr.sin_port = htons(port_number);

	// Registramos el socket
	if (bind(server_socket_descriptor, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) 
			 error("ERROR on binding");

	// Comenzamos a escuchar por el socket, 5 es la cantidad de procesos que pueden estar esperando mientra se establece la conexion.		
	listen(server_socket_descriptor,5);
	printf("-- Server listening --\n");	

	// Obtenemos el largo del mensaje
	client_lenght = sizeof(client_addr);

	// Aceptamos la conexion de algun cliente
	client_socket_descriptor = accept(server_socket_descriptor, 
				(struct sockaddr *) &client_addr, 
				&client_lenght);

	// Si no se puede obtener el descriptor, enviamos un error
	if (client_socket_descriptor < 0) 
		 error("ERROR on accept");

	// Comenzamos la lectura del mensaje	
	bzero(buffer,256);
	n = read(client_socket_descriptor,buffer,255);

	// Controlamos el error de lectura del socket
	if (n < 0) 
		error("ERROR reading from socket");
	
	// Impimimos el mensaje enviado
	printf("Here is the message: %s\n",buffer);

	// Enviamos confirmacion al cliente
	n = write(client_socket_descriptor,"I got your message",18);

	// Si no se pudo escribir, enviamos un error
	if (n < 0) 
		error("ERROR writing to socket");

	// Cerramos los descriptores
	close(client_socket_descriptor);
	close(server_socket_descriptor);

	return 0; 
}
